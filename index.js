//import modules
var fs = require('fs')
var path = require('path');
var crypt = require('./crypt/main');
var cryptfs = require('./cryptfs/main');

// get command line arguements



var argv = require('minimist')(process.argv.slice(2));
console.dir(argv.encrypt);


if (argv.encrypt && argv.pass && argv.file){
var password = argv.pass;
	//its safe to try and encrypt the file
	var file_path = argv.file;
	//read file contents synchronioulsy (seems safe for now)
	console.log("Getting file contents.");

	var contents = fs.readFileSync(file_path);

	var encrypted = crypt.encrypt(contents,password);
	
	cryptfs.saveToFile(file_path,encrypted,'encrypt');
}else if(argv.decrypt && argv.pass && argv.file){
var password = argv.pass;
	//attempt to decrypt
	var file_path = argv.file;

	var encrypted_contents = fs.readFileSync(file_path, 'utf8', function (err,data) { //for some reason had to specify for this to be read as UTF
		if (err) {
		return console.log(err);
		}
		console.log(data);
	});

	console.log(encrypted_contents);

	var decrypted = crypt.decrypt(encrypted_contents,password);

	console.log(decrypted);

	cryptfs.saveToFile(file_path,decrypted,'decrypt');	
}else if(argv.version){

	//print app version
	console.log("0.0.1")
}else{

	console.log("An Error occurred while trying to parse th command line arguements")
}
